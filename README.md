# AllStarShipsApp

_App para mostrar una gráfica de las naves de Star wars y sus propiedades_


## Despliegue en desarrollo 📦

_Para desplegar las app en modo desarrollo, ejecutar en la terminal el comando npm install seguido de npm start o ng serve_

## Construido con 🛠️

* [Angular](https://angular.io/) - El framework web Angular 11.2.7
* [PrimeNG](https://www.primefaces.org/primeng/showcase/#/) - Librerias de terceros.


## Despegado en Netlify 🖇️

* [Netlify](https://ecstatic-noether-dcb641.netlify.app/) - Hacer click en el enlace para acceder a la app desplegada.



## Autores ✒️

* **Daniel San Luis Acosta**

