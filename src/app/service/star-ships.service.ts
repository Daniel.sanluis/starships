import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { StarShips } from '../Pages/starships/interfaces/starships.interface';


@Injectable({
  providedIn: 'root'
})
export class StarShipsService {

  private urlEndPoint: string = 'https://swapi.apis.guru/?query=%20%20%20%20%20%7B%0A%20%20%20%20%20%20%20allStarships(first%3A%2030)%20%7B%0A%20%20%20%20%20%20%20starships%3Astarships%7Bid%2C%20name%2C%20crew%2C%20costInCredits%2C%20cargoCapacity%2C%20%0A%20%20%20%20%20%20%20%20hyperdriveRating%2C%20length%2C%20maxAtmospheringSpeed%2C%20passengers%7D%0A%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%7D&operationName=';

  constructor(private http: HttpClient) { }

   getStarShips(): Observable<StarShips[]> {
     return this.http.get(this.urlEndPoint).pipe( map(
       response => response['data'].allStarships.starships 
     ) )};


}
