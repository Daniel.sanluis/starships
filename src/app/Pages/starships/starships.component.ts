import { Component, Renderer2 } from '@angular/core';


@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.scss']
})


export class StarshipsComponent {

  propertie: string;

  constructor( private renderer: Renderer2) {
    renderer.setStyle(
      document.body,
      "background-image",
      "url('../../../assets/fondo_001.jpg')"
    );
  }


  handleChangePropertie(value: any): void {
    this.propertie = value;  
  }
}
