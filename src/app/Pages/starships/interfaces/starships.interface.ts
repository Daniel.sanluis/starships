export interface StarShips {
    id: string ,
    name: string ,
    crew: string | number,
    costInCredits: number,
    cargoCapacity: number,
    hyperdriveRating: number,
    length: number,
    maxAtmospheringSpeed: number,
    passengers: string | number,
  }
  