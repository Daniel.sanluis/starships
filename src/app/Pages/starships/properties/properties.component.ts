import { Component, EventEmitter, OnInit, Output } from '@angular/core';


import { Properties } from '../utils/starships.constants';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss']
})
export class PropertiesComponent implements OnInit {

  @Output() loadStarShips : EventEmitter<string> = new EventEmitter();

  properties: object[] = Properties;
  constructor() { }

  ngOnInit(): void {
  }

  handlepropertie( value ){
    this.loadStarShips.emit( value )
  }
}
