export const Properties = [
    {
      name: 'Crew' ,
      prop: 'crew',
    },
    {
      name: 'Cost in Credits' ,
      prop: 'costInCredits',
    },
    {
      name: 'Cargo Capacity' ,
      prop: 'cargoCapacity',
    },
    {
      name: 'Hyperdrive Rating' ,
      prop: 'hyperdriveRating',
    },
    {
      name: 'Length' ,
      prop: 'length',
    },
    {
      name: 'Max Atmosphering Speed' ,
      prop: 'maxAtmospheringSpeed',
    },
    {
      name: 'Passengers' ,
      prop: 'passengers',
    }, 
  ]
  
  