import { StarShips } from '../interfaces/starships.interface';

export function fixedArray( arr: StarShips[], propertie: string ): StarShips[]{
    arr.forEach(element => {
      element[propertie] = parseFloat(element[propertie] === null  ? 0 : element[propertie].toString());
      element[propertie] === null || element[propertie].toString() === 'NaN' ? element[propertie] = 0 : element[propertie];

    });
   return arr;
  }
