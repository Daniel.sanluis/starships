import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { StarShips } from '../interfaces/starships.interface';
import { Properties } from '../utils/starships.constants';
import { StarShipsService } from '../../../service/star-ships.service';
import { fixedArray } from '../utils/function.aux';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, OnChanges {

  @Input() propertie: string;

  starship: StarShips[];
  basicData: any;
  basicOptions: any = {
    title: {
        display: true,
        text: 'All Star Ships',
        fontSize: 26,
     
    },
    legend: {
        position: 'bottom',      
    }
};

  constructor(private starShipService: StarShipsService) { }

  ngOnInit(): void {
    this.loadStarShips();
  }
  ngOnChanges() {
    this.loadStarShips(this.propertie);
  }

  loadStarShips(propertie?: string): void {
    propertie ? propertie : propertie = 'crew'
    this.starShipService.getStarShips().subscribe(
      response => {
        this.starship = response
        this.basicData = {
          labels: fixedArray(response, propertie).sort((a, b) => b[propertie] - a[propertie]).map(res => res.name),
          datasets: [
            {
              label: propertie,
              backgroundColor: '#FFDE06',
              fontColor:'#FFDE06',
              data: fixedArray(response, propertie).map(res => Math.round(parseFloat(res[propertie])))
       
            },
          ]
        };

      }
    );

  }



}
