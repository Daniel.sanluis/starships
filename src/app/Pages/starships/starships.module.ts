import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StarshipsRoutingModule } from './starships-routing.module';

import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { ChartModule } from 'primeng/chart';

import { StarshipsComponent } from './starships.component';
import { ChartComponent } from './chart/chart.component';
import { PropertiesComponent } from './properties/properties.component';



@NgModule({
  declarations: [StarshipsComponent, ChartComponent, PropertiesComponent],
  imports: [
    CommonModule,
    StarshipsRoutingModule,
    ButtonModule,
    RippleModule,
    ChartModule,
  ],
  exports:[StarshipsComponent]
})
export class StarshipsModule { }
