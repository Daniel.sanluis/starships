import { Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'all-star-ships-app';
  intro: boolean = true;


  constructor(private primengConfig: PrimeNGConfig) {}


  ngOnInit() {
    this.primengConfig.ripple = true;
    this.initIntro();
  }

  initIntro():void{
    setTimeout(() => {
      this.intro = false
    }, 65000);
  }
  
}
